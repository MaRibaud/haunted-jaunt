﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tp1 : MonoBehaviour
{
    public Transform teleportTarget; //the terget for tp
    public GameObject thePlayer; //variable for player

    private void OnTriggerEnter(Collider other)
    {
        thePlayer.transform.position = teleportTarget.transform.position;
    }

}